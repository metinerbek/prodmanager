/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prodmanager;

/**
 *
 * @author dell
 */
public class Product {

    public Product(int prod_id, int prod_owner_id, int prod_category, String prod_name,String owner_name,String prod_cat_name) {
        this.prod_id = prod_id;
        this.prod_owner_id = prod_owner_id;
        this.prod_category = prod_category;
        this.prod_name = prod_name;
        this.owner_name = owner_name;
        this.prod_cat_name = prod_cat_name;
    }
    int prod_id,prod_owner_id,prod_category;
    String prod_name,owner_name,prod_cat_name;

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getProd_cat_name() {
        return prod_cat_name;
    }

    public void setProd_cat_name(String prod_cat_name) {
        this.prod_cat_name = prod_cat_name;
    }

    public int getProd_id() {
        return prod_id;
    }

    public void setProd_id(int prod_id) {
        this.prod_id = prod_id;
    }

    public int getProd_owner_id() {
        return prod_owner_id;
    }

    public void setProd_owner_id(int prod_owner_id) {
        this.prod_owner_id = prod_owner_id;
    }

    public int getProd_category() {
        return prod_category;
    }

    public void setProd_category(int prod_category) {
        this.prod_category = prod_category;
    }

    public String getProd_name() {
        return prod_name;
    }

    public void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    public float getProd_price() {
        return prod_price;
    }

    public void setProd_price(float prod_price) {
        this.prod_price = prod_price;
    }

    public float getProd_count() {
        return prod_count;
    }

    public void setProd_count(float prod_count) {
        this.prod_count = prod_count;
    }
    float prod_price,prod_count;
}
