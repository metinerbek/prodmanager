

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prodmanager;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author dell
 */
public class Conn {
 static Connection connection = null; 
 static Statement st = null;
 static ResultSet rs = null;

    public Conn() {
       connection=Conn.kontrol("promanage","postgres","toor");
    }
   
    public static Connection kontrol(String dbname,String dbusername,String dbpass){
   
    try {

			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {

			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return connection;

		}

		try {

			connection = DriverManager.getConnection(
					"jdbc:postgresql://127.0.0.1:5432/"+dbname, dbusername,
					dbpass);

		} catch (SQLException e) {

			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return connection;

		}
                return connection;
		
                
    }
    
    public static ResultSet queryWithResult(String sql){
     ResultSet rs=null;
     try {
         st = connection.createStatement();
         rs=st.executeQuery(sql);
         return rs;
         
         
     } catch (SQLException ex) {
         Logger.getLogger(Conn.class.getName()).log(Level.SEVERE, null, ex);
         return rs;
     }
     
     
     
     
    } 
    
}
