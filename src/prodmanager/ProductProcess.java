/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prodmanager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dell
 */
public class ProductProcess {

    public ArrayList<Product> getProducts() throws SQLException {
        ArrayList<Product> prods = new ArrayList<>();
        ResultSet productRs = Conn.queryWithResult(""
                + "select products.*,productowners.owner_name,product_categories.prod_cat_name from products "
                + "LEFT JOIN productowners ON productowners.owner_id=products.prod_owner_id "
                + "LEFT JOIN product_categories ON product_categories.prod_cat_id=products.prod_category"
        );
        while (productRs.next()) {
            prods.add(new Product(
                    productRs.getInt("prod_id"),
                    productRs.getInt("prod_owner_id"),
                    productRs.getInt("prod_category"),
                    productRs.getString("prod_name"),
                    productRs.getString("owner_name"),
                    productRs.getString("prod_cat_name")
        ));
          //  System.out.println(productRs.getString("prod_name"));
        }
        return prods;
    }
    
    public void productPrint(){
   
        try {
           ArrayList<Product> products= this.getProducts();
           
           System.out.println("#Ürünler\n#--------------------------------------\n");
           System.out.println("Product ID \t Product Name \t \t Product Owner \t \t Product Category ");
           System.out.println("------------------------------------------------------------------------------------------");
           for(Product prod:products){
            System.out.println(
                            prod.prod_id+" \t     | "+
                            prod.prod_name+" \t| \t" +
                             prod.owner_name+" \t| \t"+
                             prod.prod_cat_name+" \t| "
            );
               
            
          
           };
           
        } catch (SQLException ex) {
            Logger.getLogger(Prodmanager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
         System.out.println();  
    
    
    }
    
}
